Untuk membuat instance baru, lakukan: 


1. install hasura
2. eksekusi sql berikut ini: 


DROP TABLE IF EXISTS "session";
CREATE TABLE "public"."session" (
    "id" uuid DEFAULT gen_random_uuid() NOT NULL,
    "user_id" integer NOT NULL,
    "tstamp" timestamptz DEFAULT now() NOT NULL,
    CONSTRAINT "session_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "user";
DROP SEQUENCE IF EXISTS user_id_seq;
CREATE SEQUENCE user_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."user" (
    "id" integer DEFAULT nextval('user_id_seq') NOT NULL,
    "username" text NOT NULL,
    "password" text NOT NULL,
    "role" text DEFAULT 'anggota' NOT NULL,
    "fullname" text,
    "data" jsonb DEFAULT '{}' NOT NULL,
    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);



3. di hasura, buat relasi dari session ke user, dengan nama user
4. di hasura, buat permission select untuk table user, dengan kolom id, dan username untuk role `anonymous`
5. di hasura, buat permission select untuk table user, dengan semua kolom untuk semua role 